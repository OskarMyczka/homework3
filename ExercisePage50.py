#!/usr/bin/env python
""" Homework 3 page 50 - Oskar Myczka"""


class Game:
    """
    Game class for storage info
    """

    def __init__(self, name_game: str = "New Game"):
        """
        Ctor
        :param name_game:
        """

        print("Creating Game")
        self.name = name_game

    def online(self) -> None:
        """
        Ctor
        :return:
        """

        print("This is online game")

    def first_person(self) -> None:
        """
        Ctor
        :return:
        """

        print("This is first person game")


game = Game()
print(game.name)
game.online()
game.first_person()


class Genre(Game):
    """
    Genre of the game
    """

    def __init__(self):
        """
        Creating fantasy genre game
        """

        super(Genre, self).__init__("Genre")
        print("Creating fantasy genre game")

    def online(self):
        """
        Determining that the game is online
        :return:
        """

        print("This is online fantasy game")


genre = Genre()
print(game.name)
genre.online()
genre.first_person()
