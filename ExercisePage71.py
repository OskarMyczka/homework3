#!/usr/bin/env python
""" Homework 3 page 71 - Oskar Myczka"""


def check(name: str):
    """
    Check name
    :param name:
    :return:
    """
    my_name: str = "Oskar"

    if name == my_name:
        print("Hello {}".format(name))
    else:
        print("{} - intrusion!!!".format(nname))


print("Hello")
nname: str = input("Give me your name: ")
check(nname)
print()


class Rectangle:
    """
    Description rectangle
    """
    def __init__(self, length: int, width: int, name: str):
        """
        Information about the rectangle
        :param length:
        :param width:
        :param name:
        """

        self.length: int = length
        self.width: int = width
        self.name: str = name

    def __eq__(self, other):
        print("Comparision type: {} == {}".format(self.name, other.name))
        return self.length == other.length and (self.width == other.width)

    def __lt__(self, other):
        print("Comparision type: {} < {}".format(self.name, other.name))
        return self.length * self.width < other.length * other.width

    def __gt__(self, other):
        print("Comparision type: {} > {}".format(self.name, other.name))
        return self.length * self.width > other.length * other.width

    def __le__(self, other):
        print("Comparision type: {} <= {}".format(self.name, other.name))
        return self.length * self.width <= other.length * other.width

    def __ge__(self, other):
        print("Comparision type: {} >= {}".format(self.name, other.name))
        return self.length * self.width >= other.length * other.width

    def __ne__(self, other):
        print("Comparision type: {} != {}".format(self.name, other.name))
        return self.length * self.width != other.length * other.width


a = Rectangle(4, 5, "A")
b = Rectangle(5, 4, "B")

print("1. A == B = {}".format(a == b))
print("2. A < B = {}".format(a < b))
print("3. A > B = {}".format(a > b))
print("4. A <= B = {}".format(a <= b))
print("5. A >= B = {}".format(a >= b))
print("6. A != B = {}".format(a != b))

print()


class Cuboid:
    """
    Description cuboid
    """
    def __init__(self, length: int, width: int, height: int, name: str):
        """
        Information about the cuboid
        :param length:
        :param width:
        :param height:
        :param name:
        """
        self.length: int = length
        self.width: int = width
        self.height: int = height
        self.name: str = name

    def __eq__(self, other):
        print("Comparision type: {} == {}".format(self.name, other.name))
        return self.length == other.length and (self.width == other.width)

    def __lt__(self, other):
        print("Comparision type: {} < {}".format(self.name, other.name))
        return self.length * self.width * self.height < other.length * other.width * other.height

    def __gt__(self, other):
        print("Comparision type: {} > {}".format(self.name, other.name))
        return self.length * self.width * self.height > other.length * other.width * other.height

    def __le__(self, other):
        print("Comparision type: {} <= {}".format(self.name, other.name))
        return self.length * self.width * self.height <= other.length * other.width * other.height

    def __ge__(self, other):
        print("Comparision type: {} >= {}".format(self.name, other.name))
        return self.length * self.width * self.height >= other.length * other.width * other.height

    def __ne__(self, other):
        print("Comparision type: {} != {}".format(self.name, other.name))
        return self.length * self.width * self.height != other.length * other.width * other.height


A = Cuboid(4, 5, 6, "A")
B = Cuboid(5, 4, 7, "B")

print("1. A == B = {}".format(A == B))
print("2. A < B = {}".format(A < B))
print("3. A > B = {}".format(A > B))
print("4. A <= B = {}".format(A <= B))
print("5. A >= B = {}".format(A >= B))
print("6. A != B = {}".format(A != B))
