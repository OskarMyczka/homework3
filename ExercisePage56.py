#!/usr/bin/env python
""" Homework 3 page 56 - Oskar Myczka"""

print("Hello")
name: str = input("What is your name? ")
surname: str = input("What is your surname? ")
age: str = input("How old are you? ")

age: int = int(age)
print()

print("Your name:", name)
print("Your surname:", surname)
print("Your age:", age)
print()

number: str = input("Give me a number: ")

number: float = float(number)

print(isinstance(number, float))

print(number)
print()


class Flower:
    """
    A class that stores flower information
    """

    def __init__(self, flower_type, flower_colour):
        """
        Ctor
        :param flower_type:
        :param flower_colour:
        """
        self.flower_type = flower_type
        self.flower_colour = flower_colour

    def describe_flower(self):
        """
        Describle flower
        :return:
        """
        print("Your flower type is {} and have {} colour".format(self.flower_type, self.flower_colour))


raw_flower_type: str = input("What type is your flower? ")
raw_flower_colour: str = input("What color is your flower? ")

flower = Flower(raw_flower_type, raw_flower_colour)
flower.describe_flower()
