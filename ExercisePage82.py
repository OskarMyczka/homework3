#!/usr/bin/env python
""" Homework 3 page 82 - Oskar Myczka"""

list1 = list([1])
list2 = list([5])
list3 = list([10])
my_list = [list1, list2, list3]
print(my_list)
print()

empty_list = list()

for empty_list in range(100, 115):
    print(empty_list, end=" ")

print()
print()


list4 = list([2.15, 3.50, 4.23, 9.13, 10.67])

for num in list4:
    print(num, end=" ")

print()
