#!/usr/bin/env python
""" Homework 3 page 41- Oskar Myczka"""


def name():
    """
    Printing my name
    """
    print("Your name is Oskar")


def addition(a: int, b: int, c: int) -> int:
    """
    adding three numbers
    """
    return a + b + c


def world() -> str:
    """
    Priting hello world
    :return:
    """

    return "Hello world"


def subtraction(number: int) -> int:
    """
    substraction 2 numbers
    :param number:
    :return:
    """
    return 5 - number


class Home:
    pass


def create_home() -> Home:
    """
    Creating object in class
    :return:
    """

    return Home()


name()
print()

print(addition(2, 4, 6))
print()

world = world()
print(world)
print()

print(subtraction(4))
print()

new_home: Home = create_home()

print(new_home)
print(isinstance(new_home, Home))
